import VueRouter from 'vue-router'

import HousesBuyPage from '../components/HousesBuyPage'
import StartPage from '../components/StartPage'
import OrderConfirmationPage from '../components/OrderConfirmationPage'
import FinalPage from '../components/FinalPage'
import HousesCartItem from '../components/HousesCartItem'


export default  new VueRouter({
    mode: 'history',
    routes:[
        {  
          path:'/houses',
          component: HousesBuyPage, 
          props: true             
          
    
       },

       {  
        path:'/',
        component: StartPage
        
  
     },

     {  
        path:'/Order',
        name:'Order',
        component: OrderConfirmationPage,
        props: true,
        
        children: [{
           path: '/HousesItem',
           name: 'HousesItem',
           component:HousesCartItem

         } ]
  
     },

     {  
        path:'/Final',
        component: FinalPage,
        props:true
  
     }
    ]
})